# Upload to the OSSR

 - Uses the GitLab CI to upload the current project to the ESCAPE OSSR (The ESCAPE2020 Zenodo community).
    - Note that the CI will be only triggered with the creation of a new release.
 - The `codemeta.json` file - compulsory if you want to run this code - will be also added to the record as
 a separate file.
 - The `eossr-check-connection-zenodo` stage will create a fist dummy upload (that will be always erased), to check
 that the released code will be successfully uploaded.

## Important note

The first time CI is run, remove `-id $ZENODO_PROJECT_ID` to create a new record.
After that, **you need to create the CI variable** `$ZENODO_PROJECT_ID` (or `$SANDBOX_ZENODO_PROJECT_ID`)
using the newly created record ID.
**If you don't,** the next release will create a new record instead of updating the existing one.

To create the CI `$(SANDBOX_)ZENODO_PROJECT_ID` variable:
1. Go to https://zenodo.org/deposit or https://sandbox.zenodo.org/deposit;
2. Click onto your just uploaded project,
3. From your browser search bar, **just** copy the number (your `deposit id`) that it is included in the https direction.
   - ex: `https://zenodo.org/record/3884963` --> just copy `3884963`.
4. Save it as a new environment variable in your GitLab project.
    * Go to your GitLab project.
    * Click on `Settings` --> `CI/CD` --> `Variables` --> `Add variable`
    * `KEY`=`$ZENODO_PROJECT_ID` and fill the value with the deposit id.
    * For Sandbox Zenodo use `KEY` = `SANDBOX_ZENODO_PROJECT_ID`


## Upload to Zenodo

```yaml
stages:
 - deploy

deploy_zenodo:
  stage: deploy
  image: registry.gitlab.com/escape-ossr/eossr:v2.0.0
  before_script:
    - eossr-check-connection-zenodo --token $ZENODO_TOKEN -p $CI_PROJECT_DIR
  script:
    - eossr-upload-repository --add-escape2020 -t $ZENODO_TOKEN -i $CI_PROJECT_DIR --archive-name $CI_PROJECT_NAME.zip [-id $ZENODO_PROJECT_ID]
  only:
    - tags

```

## Upload to Sandbox Zenodo

```yaml
stages:
 - deploy

deploy_zenodo:
  stage: deploy
  image: registry.gitlab.com/escape-ossr/eossr:v2.0.0
  before_script:
    - eossr-check-connection-zenodo --token $SANDBOX_ZENODO_TOKEN -p $CI_PROJECT_DIR --sandbox
  script:
    - eossr-upload-repository --add-escape2020 -t $SANDBOX_ZENODO_TOKEN --sandbox -i $CI_PROJECT_DIR --archive-name $CI_PROJECT_NAME.zip [-id $SANDBOX_ZENODO_PROJECT_ID]
  only:
    - tags
```

Note: you might want to restore the directory to its tag stage if you made changes in previous CI stages
(for example removing build subdirs from previous test stages).
You may do so using adding the following in the `before_script` section:
```
- apt-get install git -y
- git reset --hard HEAD && git clean -fd -x
```
