eossr.api.zenodo package
========================


eossr.api.zenodo.http\_status module
------------------------------------

.. automodule:: eossr.api.zenodo.http_status
   :members:
   :undoc-members:
   :show-inheritance:

eossr.api.zenodo.zenodo module
------------------------------

.. automodule:: eossr.api.zenodo.zenodo
   :members:
   :undoc-members:
   :show-inheritance:
