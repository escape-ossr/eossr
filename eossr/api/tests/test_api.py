import os
import pytest
from eossr import api
from eossr.api.ossr import get_ossr_pending_requests


def test_search_ossr_records():
    ossr_records = api.search_ossr_records(all_versions=True)
    assert len(ossr_records) >= 12  # number of records October 01, 2021
    all_ids = [rec.data['id'] for rec in ossr_records]
    assert 5524913 in all_ids  # id of the version v0.2 of the eossr


@pytest.mark.skipif(os.getenv('ZENODO_TOKEN') is None, reason="ZENODO_TOKEN not defined")
def test_get_ossr_pending_requests():
    # Use the actual ZenodoAPI class
    zenodo_token = os.getenv('ZENODO_TOKEN')

    # Call the function with a dummy token
    result = get_ossr_pending_requests(zenodo_token)

    # Assert that the function returns the expected result
    assert isinstance(result, list)
    assert all(isinstance(item, api.zenodo.zenodo.PendingRequest) for item in result)
